source /cvmfs/sft.cern.ch/lcg/views/LCG_105/x86_64-el9-gcc13-opt/setup.sh 

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
setupATLAS

echo "Now setting up tensorflow via LCG_105..."
lsetup "lcgenv x86_64-el9-gcc13-opt tensorflow -p LCG_105" &> /dev/null
lsetup "lcgenv x86_64-el9-gcc13-opt matplotlib -p LCG_105" &> /dev/null
lsetup "lcgenv x86_64-el9-gcc13-opt pandas -p LCG_105" &> /dev/null
lsetup "lcgenv x86_64-el9-gcc13-opt scikitlearn -p LCG_105" &> /dev/null