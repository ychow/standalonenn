import os
import sys
import math
from datetime import datetime, date
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow import keras
from sklearn.model_selection import train_test_split

def split(df):
    train_df, test_df = train_test_split(df, test_size=0.5, shuffle=True)
    return train_df, test_df

class NNModel():
    def __init__(self,output_dir="TestCase"):
        self.output_dir = output_dir
        self.df_train = pd.DataFrame()
        self.df_test = pd.DataFrame()
        self.eval_df = pd.DataFrame()
        self.normalizer = None
        self.numeric_features = []
        self.numeric_feature_names = []        
        self.target_var = ""
        self.target = pd.DataFrame()
        
    def set_features(self, features=[]):
        self.numeric_feature_names = features
        
    def set_target(self, target_var=""):
        self.target_var = target_var
        self.target = self.train_df.pop(self.target_var)
        
    def build_basic_model(self):
        feature_count = len(self.numeric_feature_names)  # Determine the number of input features
        self.model = tf.keras.Sequential([
            # Commented out the normalizer for now
            # if self.normalizer is not None:
            #     self.model.add(self.normalizer),
            tf.keras.layers.InputLayer(input_shape=(feature_count,)),  # Add an input layer specifying the shape
            tf.keras.layers.Dense(10, activation='selu'),
            tf.keras.layers.Dense(10, activation='selu'),
            tf.keras.layers.Dense(5, activation='selu'),
            tf.keras.layers.Dense(1)
        ])
        adam = tf.keras.optimizers.Adam(learning_rate=0.01)
        self.model.compile(optimizer=adam,
                        loss='mean_squared_error',
                        metrics=[tf.metrics.MeanAbsoluteError()])

    def build_normalization(self):
        self.numeric_features = self.train_df[self.numeric_feature_names]
        self.numeric_features.head()
        print(tf.convert_to_tensor(self.numeric_features))
        self.normalizer = tf.keras.layers.Normalization(axis=-1)
        self.normalizer.adapt(self.numeric_features)
        self.normalizer(self.numeric_features)

    def train(self, epochs=10):
        if self.train_df.empty:
            print("INVALID INPUT")
            return
        # self.build_normalization()
        self.build_basic_model()
        self.model.summary()   
        BATCH_SIZE = 256
        c1 = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=5)
        c2 = tf.keras.callbacks.EarlyStopping(monitor='val_mean_absolute_error', patience=5)
        self.history = self.model.fit(self.train_df[self.numeric_feature_names],self.target, 
                                      validation_split = 0.3, epochs=epochs, batch_size=BATCH_SIZE, callbacks=[c1,c2])
        
    def plot_history(self):
        history = self.history
        plt.figure()
        plt.yscale('log')
        plt.plot(history.history['mean_absolute_error'])
        plt.plot(history.history['val_mean_absolute_error'])
        plt.title('mean_absolute_error')
        plt.ylabel('mean_absolute_error')
        plt.xlabel('epoch')
        plt.legend(['train', 'val'], loc='upper left')
        plt.savefig(self.output_dir+'/mean_absolute_error.png', dpi=300)
        plt.show()

        plt.figure()
        plt.yscale('log')
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        plt.title('model loss')
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['train', 'val'], loc='upper left')
        plt.savefig(self.output_dir+'/loss.png', dpi=300)
        plt.show()

    def predict(self, df=pd.DataFrame(), col_predict="pred"):
        if df.empty:
            df = self.df_train
        x = df[self.numeric_feature_names]
        df[col_predict] = self.model.predict(x)
        df[col_predict+"_ABSErr"] = (df[self.target_var] - df[col_predict]).abs()
        return df

