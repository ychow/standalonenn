import sys
sys.path.append('../')
from models.NN import *

def run_test_case():
    n_size = 1000
    np.random.seed(42)  # For reproducibility
    df = pd.DataFrame({
        'jet1pt': np.random.rand(n_size),
        'jet2pt': np.random.rand(n_size),
        'tau1pt': np.random.rand(n_size),
        'tau0pt': np.random.rand(n_size),
        'MET': np.random.rand(n_size)
    })
    # testing, need not to make physical sense
    df['MET'] = 1+df['tau1pt'] + df['tau0pt'] - df['jet1pt'] - df['jet2pt'] + np.random.normal(0.1, 0.2, size=df.shape[0])

    df.describe()

    features=["jet1pt","jet2pt","tau1pt","tau0pt"]
    target = "MET"

    training_set, testing_set = split(df)
    
    os.makedirs("TestCase", exist_ok=True)
    NN = NNModel(output_dir="TestCase")
    NN.train_df = training_set
    NN.set_target(target)
    NN.set_features(features)
    NN.train(epochs=50)
    NN.plot_history()

    prediction = NN.predict(testing_set, col_predict="pred")
    return prediction

prediction = run_test_case()
print(prediction.tail(n=5))
print(prediction.describe())