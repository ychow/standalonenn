import pandas as pd
import numpy as np
import uproot
import time

# Flatten array column function
def flatten_array_column(df, column_name, max_cols=2):
    def expand_and_pad(row):
        if row is None or (isinstance(row, float) and np.isnan(row)):
            return [np.nan] * max_cols
        row_list = list(row)
        return row_list[:max_cols] + [np.nan]*(max_cols - len(row_list[:max_cols]))

    expanded = df[column_name].apply(expand_and_pad)
    expanded_df = pd.DataFrame(expanded.tolist(), columns=[f'{column_name}{i}' for i in range(max_cols)])
    df_flattened = pd.concat([df.drop(column_name, axis=1), expanded_df], axis=1)
    return df_flattened

def get_df_from_root_files(file_path, tree_name, branch_names, run_flattern=True):
    start_time = time.time()

    with uproot.open(file_path) as file:
        arrays = file[tree_name].arrays(branch_names, library="np")
    df = pd.DataFrame({name: arrays[name] for name in arrays.keys()})

    if run_flattern:
        for branch in branch_names:
            df = flatten_array_column(df, branch, max_cols=2)

    uproot_time = time.time() - start_time
    print("uproot approach took: {:.2f} seconds".format(uproot_time))
    
    return df