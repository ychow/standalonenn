import sys
sys.path.append('../')
from models.NN import *
from RootFileDataFrameConverter import *

# file_path = '/afs/cern.ch/user/y/ychow/WorkLink/public/ForHNL/Samples/TPXGFW1Out/500558/tpxout.ntuple.500558.mc20a.root'
# tree_name = 'nominal'
# folder = "/user/cedwin/HNL/PrivateEarlyVBSssWWtautau/StandAloneScripts/TruthNtuples/signal_ntuple/combined/"
folder = "/afs/cern.ch/user/y/ychow/WorkLink/public/ForHNL/Samples/TRUTHNTUPLE/"
branch_names = ['jet0_pt', 'jet1_pt','jet0_eta','jet1_eta','jet0_phi', 'jet1_phi','tau0_pt_vis','tau0_eta_vis', 'tau1_pt_vis','tau1_eta_vis', 'tau0_phi_vis','tau1_phi_vis', "m_tautau", "m_tautau_vis"]

features = [k+"0" for k in branch_names]
features.remove("m_tautau0")
features.remove("m_tautau_vis0")

original_var = "m_tautau_vis0"
target = "m_tautau0"
tree_name = "Nominal/tree"
prediction_column = "pred_mtautau"

def train_NN():
    file_path = folder+"500556.root"
    
    df = get_df_from_root_files(file_path, tree_name, branch_names, run_flattern=True)

    df = df.sample(20000)
    print(df.describe())

    training_set, testing_set = split(df)

    os.makedirs("HNLTruth", exist_ok=True)
    NN = NNModel(output_dir="HNLTruth")
    NN.train_df = training_set
    NN.set_target(target)
    NN.set_features(features)
    NN.train(epochs=50)
    NN.plot_history()
    
    return NN, training_set, testing_set

def get_truth_testing_set(file_path):
    df = get_df_from_root_files(file_path, tree_name, branch_names, run_flattern=True)
    df = df.sample(20000)
    return df

NN, training_set, testing_set = train_NN()
testing_set = NN.predict(testing_set, col_predict=prediction_column) #10TeV
print(testing_set.describe())

df_0p5TeV = get_truth_testing_set(folder+"500557.root")#500
df_0p5TeV = NN.predict(df_0p5TeV, col_predict=prediction_column)
print(df_0p5TeV.describe())

df_1TeV = get_truth_testing_set(folder+"500558.root")#1T
df_1TeV = NN.predict(df_1TeV, col_predict=prediction_column)
print(df_1TeV.describe())

df_3TeV = get_truth_testing_set(folder+"500559.root")#3T
df_3TeV = NN.predict(df_3TeV, col_predict=prediction_column)
print(df_3TeV.describe())

df_10TeV = get_truth_testing_set(folder+"500556.root")#10T
df_10TeV = NN.predict(df_10TeV, col_predict=prediction_column)
print(df_10TeV.describe())



#plot predict vs true distribution

bins=np.linspace(0, 5e6, 101)

plt.figure()
plt.hist(df_0p5TeV[target], bins=bins, alpha=0.5, label="500GeV True")
# plt.hist(df_0p5TeV[prediction_column], bins=bins, alpha=0.5, label="500GeV Predicted")

plt.hist(df_1TeV[target], bins=bins, alpha=0.5, label="1TeV True")
# plt.hist(df_1TeV[prediction_column], bins=bins, alpha=0.5, label="1TeV Predicted")

plt.hist(df_3TeV[target], bins=bins, alpha=0.5, label="3TeV True")
# plt.hist(df_3TeV[prediction_column], bins=bins, alpha=0.5, label="3TeV Predicted")

plt.hist(df_10TeV[target], bins=bins, alpha=0.5, label="10TeV True")
plt.legend(loc='upper right')
plt.xlabel(r'$m_{\tau\tau}$ [TeV]')
plt.savefig("HNLTruth/compare.png", dpi=300)
plt.show()


bins=np.linspace(0, 3e6, 101)
plt.figure()
plt.hist(df_10TeV[original_var], bins=bins, alpha=0.5, label="10TeV TauVis")
plt.hist(df_10TeV[target], bins=bins, alpha=0.5, label="10TeV TauTrue")
plt.hist(df_10TeV[prediction_column], bins=bins, alpha=0.5, label="10TeV NN Predicted")
plt.legend(loc='upper right')
plt.xlabel(r'$m_{\tau\tau}$ [TeV]')
plt.xlim(xmin=0,xmax=3e6)
plt.savefig("HNLTruth/predict_vs_true10.png", dpi=300)
plt.show()

plt.figure()
plt.hist(df_3TeV[original_var], bins=bins, alpha=0.5, label="3TeV TauVis")
plt.hist(df_3TeV[target], bins=bins, alpha=0.5, label="3TeV TauTrue")
plt.hist(df_3TeV[prediction_column], bins=bins, alpha=0.5, label="3TeV NN Predicted")
plt.legend(loc='upper right')
plt.xlabel(r'$m_{\tau\tau}$ [TeV]')
plt.xlim(xmin=0,xmax=3e6)
plt.savefig("HNLTruth/predict_vs_true3.png", dpi=300)
plt.show()

plt.figure()
plt.hist(df_1TeV[original_var], bins=bins, alpha=0.5, label="1TeV TauVis")
plt.hist(df_1TeV[target], bins=bins, alpha=0.5, label="1TeV TauTrue")
plt.hist(df_1TeV[prediction_column], bins=bins, alpha=0.5, label="1TeV NN Predicted")
plt.legend(loc='upper right')
plt.xlabel(r'$m_{\tau\tau}$ [TeV]')
plt.xlim(xmin=0,xmax=3e6)
plt.savefig("HNLTruth/predict_vs_true1.png", dpi=300)
plt.show()