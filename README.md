# StandAloneNN

Testing NN toy model without any need of data file.

## Setup
You may need to find out your server setting and change the setup script!

You can also let me know about your os for support.

Current setup use tensorflow under LCG_105

## Directly running the NN on lxplus9
source setup_lxplus9.sh

cd run

python run_test_case.py

python run_truth_NN.py

## Directly running the NN on nikhef machines
source setup_nikhef_centos7.sh  

cd run

python run_test_case.py

